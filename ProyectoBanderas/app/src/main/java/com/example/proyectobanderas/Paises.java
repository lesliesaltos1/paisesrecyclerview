package com.example.proyectobanderas;

public class Paises {
    private String name;
    private String info;
    private String foto;
    private String url;
    private String capital;
    private double latitud;
    private double longitud;

    public Paises(String name, String info, String foto, String url, String capital, double latitud, double longitud) {
        this.name = name;
        this.info = info;
        this.foto = foto;
        this.url = url;
        this.capital = capital;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Paises(String name, String info, String foto, String url, String capital) {
        this.name = name;
        this.info = info;
        this.foto = foto;
        this.url= url;
        this.capital=capital;
    }
    public Paises(String name, String info, String foto) {
        this.name = name;
        this.info = info;
        this.foto = foto;

    }

    public Paises() {

    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
