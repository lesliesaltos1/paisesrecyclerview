package com.example.proyectobanderas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Helper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.proyectobanderas.databinding.ActivityMainBinding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    //RecyclerView recyclerView;
    //ArrayList<Paises>paises;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);

        //recyclerView=(RecyclerView)findViewById(R.id.recId);
        binding.recId.setHasFixedSize(true);
        binding.recId.setLayoutManager(new LinearLayoutManager(this));
         List<Paises> paises = llenar();
        AdapterBanderas mAdapter = new AdapterBanderas(paises);
        binding.recId.setAdapter(mAdapter);

    }
    protected List<Paises> llenar(){
        List<Paises> paises=new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("lista.csv")));
            String line;
            Log.e("Reader Stuff",reader.readLine());

            while ((line = reader.readLine()) != null) {
                Log.e("code",line);
                String[] d = line.split(";");
                paises.add(new Paises(d[0],d[1],d[2],d[3],d[4],Double.parseDouble(d[5]),Double.parseDouble(d[6])));
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }


        return paises;
    }
}